"""
By Razvan Ciuca, 2017

This script computes the quantities necessary to rescale the maps and compute the posteriors in bayesian.py

"""


import torch as t
from torch.autograd import Variable
import torch.nn.functional as F
import torch.nn as nn
import numpy as np
import numpy.fft as fft
import pickle
import scipy.misc


from feeder import DataFeeder
from model_def import Net
from utils import *
from config import *


# this is the function which computes frequencies and saves them, we also save statistics about the
# histograms of output maps
def compute_frequencies(model, save_to=also_save_frequencies_to):

    # dictionary which will contain, for each Gmu, the h_Gmu(x) values used to rescale the predictions,
    # We approximate the function h_Gmu(x) as a step function i.e. we only store the values x_i and h_Gmu(x_i)
    # frequencies[Gmu] contains an array of the form [intervals, h_Gmu], intervals = [x_0, x_1, ... x_n],
    # h_Gmu = [h_Gmu(x_0) ... h_Gmu(x_n)], where n = divisions below
    frequencies = {}
    divisions = global_f_divisions
    # largest/smallest Gmu for which to compute frequencies
    starting_gmu = global_starting_gmu
    final_gmu = global_final_gmu
    # number of gmu steps we take in log-space
    gmu_divisions = global_gmu_divisions
    border_size = border

    histograms = {}
    histogram_divisions = 1000

    # load the maps we use
    if frequency_calculation_string_type == 'cmbedge':
        g_maps = np.load(cmbedge_g_frequency_calculation_filename)
        s_maps = np.load(cmbedge_s_frequency_calculation_filename)
        a_maps = np.load(cmbedge_a_frequency_calculation_filename)
        a_maps = a_maps[:, :, border_size:-border_size, border_size:-border_size]
    else:
        print("must set string_type to cmbedge")

    # number of total maps for which we compute predictions, if we're on gpu, use all maps, if not, only use 10
    total_maps = g_maps.shape[0] if t.cuda.is_available() else 10

    # the prior probability of a pixel being on a string is just the mean of a_maps, we compute this here
    # even though it's only used in bayesian.py to avoid having to load the a_maps there again
    prior = a_maps.mean()

    for Gmu in [starting_gmu * (final_gmu/starting_gmu)**(i/gmu_divisions) for i in range(0, gmu_divisions)]:

        print("doing Gmu=" + str(Gmu))

        # create sky maps by combining g_maps with s_maps
        maps = Variable(t.from_numpy(normalize_map(g_maps + Gmu*s_maps)))

        # if possible use the gpu
        if t.cuda.is_available():
            maps = maps.cuda()

        # total_maps is likely too large to compute all the predictions at once, even on gpu, so we split the dataset
        # and compute predictions in chunks of n at a time

        n = 20 if t.cuda.is_available() else 1
        predictions = [model.forward(maps[i:i+n]).data[:, :, border_size:-border_size, border_size:-border_size]
                       for i in range(0, total_maps, n)]
        # concatenate the predictions and convert to numpy
        predictions = t.cat(predictions, 0).cpu().numpy()

        # compute histogram of values for this Gmu
        # hist, intervals = np.histogram(predictions, divisions)
        hist, intervals = constant_bin_height_histogram(predictions, divisions)

        frequencies[Gmu] = [intervals, []]

        # iterate over the divisions and find what fraction of pixels containing values between intervals[i] and
        # intervals[i+1] are actually on a string
        for i in range(0, divisions):
            mask = (predictions >= intervals[i]) * (predictions < intervals[i+1])
            mask_sum = mask.sum()
            frequencies[Gmu][1].append((mask * a_maps[0:total_maps]).sum() / mask_sum)

        # -------- frequency calculation complete, now doing histogram calculation ---------

        temp_histograms = []
        inter = []
        sigmoid_predictions = 1/(1+np.exp(-predictions))

        # for each prediction map, compute the histogram of its values and store it in temp_histograms
        for m in sigmoid_predictions:
            hist, inter = np.histogram(m, histogram_divisions, range=(0, 1))
            temp_histograms.append(hist.reshape([1, -1]))

        # compute, for each histogram bin, the mean and standard deviation across the dataset of predictions maps
        hists = np.concatenate(temp_histograms, axis=0)
        hist_means = np.mean(hists, axis=0)
        hist_stds = np.std(hists, axis=0)
        histograms[Gmu] = [hist_means, hist_stds, inter]

    if save_to is not None:
        pickle.dump([frequencies, prior], open(save_to + '/frequencies_' + frequency_calculation_string_type, 'wb'))
        pickle.dump(histograms, open(save_to + '/histograms_' + frequency_calculation_string_type, 'wb'))

    return frequencies, prior, histograms


if __name__ == '__main__':

    # change the model_filename in config to the one you want to use
    model_filename = compute_frequencies_for_this_model_filename
    model = Net()
    model.load(model_filename)

    if t.cuda.is_available():
        model = model.cuda()

    freq, prior, histograms = compute_frequencies(model)








