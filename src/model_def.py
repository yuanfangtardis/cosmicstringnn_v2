"""
by Razvan Ciuca, 2017,

This file contains the definitions of the neural network models we are using, right now it contains 2 different classes
of models:

Net : a network with 5 convolution layers, it is the one used in arXiv:1706.04131 and described in arXiv:1708.08878

NEW FUNCTIONALITY:

1. The class Net and other future networks inherit from the defined class CosmicStringsModel, this allows it to share the same
saving/loading routines, any models further defined can also inherit from CosmicStringsModel to copy this functionality.
note that the saving/loading routines now use pickles objects, this is because we need to story the model.network_description,
model.history and model.frequencies, these are not automatically stored by the pytorch routines used in earlier versions

2. We defined a function load_model which is capable of recognizing which type of model it is loading, making model
loading much easier

3. We implemented a model.history variable which logs training runs details and frequency computation details, here
model.history is an array and model.history[0] is the first thing that happened to the network, model.history[-1]
is the last

4. now all frequency calculations with a given model automatically store their data in model.frequencies, in the case
where multiple frequency calculations are made with the same model, they are appended to the model.frequencies array. So
model.frequencies[0] contains the oldest frequencies for this model, for details of the parameters used for that 
specific frequency calculation, see model.history

5. Now you can feed numpy arrays or pytorch tensors of the wrong format to model.forward and it will correctly 
change them to the right format, however, the output is still a pytorch Variable wrapping a tensor of size [B, 1, N, N]


"""


import numpy as np
import torch as t
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable
#oh#from feeder import DataFeeder
from scipy.misc import imshow
import pickle


# makes the input into the correct format for feeding to the network
# accepts numpy maps, tensors and Variables of shape [N, N], [N, N], [B, N, N], [B, 1, N, N] and converts them all to
# Variables of shape [B, 1, N, N]
def convert_input_format(x):

    # is it a tensor? yes, then wrap it in Variable and convert the size
    if t.is_tensor(x):
        if len(x.size()) == 2:
            output = x.unsqueeze(0).unsqueeze(0)
        elif len(x.size()) == 3:
            output = x.unsqueeze(1)

        output = Variable(output)

    # not a tensor? then it must be either a Variable or a numpy array
    else:
        # is it a variable?
        if type(x).__module__ == 'torch.autograd.variable':
            if len(x.size()) == 2:
                output = x.unsqueeze(0).unsqueeze(0)
            elif len(x.size()) == 3:
                output = x.unsqueeze(0)
            else:
                output = x
        elif type(x).__module__ == 'numpy':
            output = t.from_numpy(x.astype(np.float32))
            if len(output.size()) == 2:
                output = output.unsqueeze(0).unsqueeze(0)
            elif len(output.size()) == 3:
                output = output.unsqueeze(1)
            output = Variable(output)

    return output


# class defining a convolution operation with the assumption of periodic boundary conditions
class RepeatingConv(nn.Module):

    def __init__(self, in_channels, out_channels, conv_size):
        super(RepeatingConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, conv_size, 1, 0)
        self.conv.weight.data /= 10
        self.conv_size = conv_size

    def pad_repeating(self, x, pad_size):
        s2 = x.size(2)
        s3 = x.size(3)
        x1 = t.cat([x[:, :, s2 - pad_size: s2], x, x[:, :, 0:pad_size]], 2)
        x2 = t.cat([x1[:, :, :, s3 - pad_size: s3], x1, x1[:, :, :, 0:pad_size]], 3)
        return x2

    def forward(self, x):
        if self.conv_size == 1:
            return self.conv(x)
        else:
            x = self.pad_repeating(x, int((self.conv_size-1)/2))
            x = self.conv(x)
            return x


# mainly provides uniform saving/loading and history logging facilities
# the model.history is an array containing stuff that happened to the model in chronological order
# Our network Net and other future networks descend from it
class CosmicStringsModel(nn.Module):

    def __init__(self):
        super(CosmicStringsModel, self).__init__()
        self.history = []
        self.frequencies = []

    # used to log a training run into model.history
    def add_training_run_to_history(self, n_iterations, Gmu, string_type, noise_std, batch_size, learning_rate, optimization_goal):

        training_run = {'n_iterations': n_iterations,
                        'Gmu': Gmu,
                        'string_type': string_type,
                        'noise_std': noise_std,
                        'batch_size:': batch_size,
                        'learning_rate:': learning_rate,
                        'optimization_goal': optimization_goal}

        self.history.append(training_run)

    # used to add an iteration to the latest training run
    def add_iteration_to_current_training_run(self, n_iterations_to_add=1):
        self.history[-1]['n_iterations'] += n_iterations_to_add

    # add a frequencies object to the array and append to history to mention what was computed
    def add_frequencies(self, frequencies, string_type, other_comments):
        self.frequencies.append(frequencies)
        self.history.append('computed new frequencies with string type ' + string_type +
                             ', appended to model.frequencies, other comments: ' + other_comments)

    # saves the current weights to filename
    def save(self, filename):
        to_save = [self.network_description, self.frequencies, self.history, self.state_dict()]
        pickle.dump(to_save, open(filename, 'wb'))

    # loads the weights from filename into the current net
    def load(self, filename):
        self.network_description, self.frequencies, self.history, state_dict = pickle.load(open(filename, 'rb'))
        self.load_state_dict(state_dict)


# function for generalized model loading, if you define more models you need to add them here
# each model has a model.network description variable which contains a decription of which it is, we use that variable
# to detect which model it is
def load_model(filename):

    network_description, frequencies, history, state_dict = pickle.load(open(filename, 'rb'))

    if network_description.split('_')[0] == 'Net':
        model = Net()
    else:
        print('sorry, network type is not recognized')
        return None

    model.frequencies = frequencies
    model.history = history
    model.load_state_dict(state_dict)

    return model


def convert_from_old_model_format(model_filename, frequencies_filename, model_type):
    
    if model_type.split('_')[0] == 'Net':
        model = Net()
    else:
        print('model type must be Net')
        return 0
    
    model.load_state_dict(t.load(model_filename, map_location=lambda storage, loc: storage))
    model.frequencies.append(pickle.load(open(frequencies_filename, 'rb')))
    
    return model



# model defining a primitive network used in the first paper
class Net(CosmicStringsModel):
    def __init__(self):
        super(Net, self).__init__()
        conv_size = 3
        channels = 32
        self.network_description = 'Net'
        # each of these self.conv is an instance of the RepeatingConv class
        self.conv1 = RepeatingConv(1, channels, conv_size)
        self.conv2 = RepeatingConv(channels, channels, conv_size)
        self.conv3 = RepeatingConv(channels, channels, conv_size)
        self.conv4 = RepeatingConv(channels, channels, conv_size)
        self.conv7 = nn.Conv2d(channels, 1, 1, 1, 0)
        # the last one is named with 7 instead of 5 because of backward compatibility with older versions,
        # changing it will cause ./starting_model.pth to no longer be loadable

    # this is the function where the network operations actually take place
    def forward(self, x):
        x = convert_input_format(x)
        x = F.tanh(self.conv1.forward(x))
        x = F.tanh(self.conv2.forward(x))
        x = F.tanh(self.conv3.forward(x))
        x = F.tanh(self.conv4.forward(x))
        x = self.conv7(x)

        return x

# if you are calling this one directly, it must be because of troubleshooting, so here's a piece of code which
# shows roughly how to use the code in this script
if __name__ == '__main___':

    # instantiate a model, this is not a trained network and will have random weights
    model = Net()

    # if we want to transfer the model from cpu to gpu or from gpu to cpu, we use the following lines, we need a
    # model which lives in gpu to evaluate it on maps in gpu memory
    if t.cuda.is_available():  # verifies if gpu is available on the current machine
        model = model.cuda()  # transfers the model to gpu memory
        model = model.cpu()  # transfers it back to cpu memory

    # instantiate a feeder
    feeder = DataFeeder()

    # get a data batch from the feeder
    inputs, answers = feeder.get_batch(batch_size=1, Gmu=2e-8, noise=None, random_indices=False,
                                       gpu_flag=False, string_type='cmbedge')

    # evaluate the model on the inputs, note that here the outputs are not between 0 and 1, we need to apply the sigmoid
    # to get proper probabilities
    outputs = model.forward(inputs)
    predictions = t.sigmoid(outputs)  # simply applies 1/(1+exp(-x)) element-wise to outputs

    # next view the outputs using imshow, the outputs should be pretty bad since the network is untrained
    imshow(outputs.data.numpy()[0, 0])









